import React, { useRef, useEffect, useState } from "react";
import { Form, Row, Button, Table, Col } from "react-bootstrap";
import {deleteAuthor,postAuthor,putAuthor,fetchAuthor,uploadImage,} from "../services/author_service";
export default function Author() {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const tempId = useRef('');
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [isUpdate, setIsUpdate] = useState(false);

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    let authors = await fetchAuthor();
    setAuthor(authors);
  };

  const onAdd = async (e) => {
    e.preventDefault();
    let tempAuthor = { name: name, email: email };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      tempAuthor.image = url;
    }
    const addNewAuthor = async () => {
        let response = await postAuthor(tempAuthor);
        alert(response);
        fetch();
      }
    addNewAuthor();
    setImageFile(null);
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  };

  function onDelete(id) {
    deleteAuthor(id);
    let deletedAuthorArr = author.filter((item) => item._id !== id);
    setAuthor(deletedAuthorArr);
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  const onUpdate = async (e) => {
    e.preventDefault();
    let TempAuthor = { name: name, email: email };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      TempAuthor.image = url;
    }
    const updateAuthor = async () => {
      try {
        const response = await putAuthor(tempId.current, TempAuthor);
        alert(response);
        fetch();
      } catch (err) {
        alert(err);
      }
    };
    setIsUpdate(false);
    setImageFile(null);
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "";
    updateAuthor();
  };

  return (
    <div className="container" >
      <h1>Author</h1>
      <Row>
        <Col sm={8}>
          <Form>
            <Form.Group>
              <Form.Control
                id="name"
                type="text"
                name="name"
                placeholder="Author Name"
                onChange={(event) => {
                  setName(event.target.value);
                }}
              />
              <Form.Text className="text-danger"></Form.Text>
            </Form.Group>

            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                id="email"
                type="text"
                name="email"
                placeholder="Email"
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
              ></Form.Control>
              <Form.Text className="text-danger"></Form.Text>
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={isUpdate ? onUpdate : onAdd}
            >
              {isUpdate ? "Update" : "Add"}
            </Button>
          </Form>
        </Col>
        <Col md={4} >
          <img id="image" className="w-75" src={imageURL} />
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Table striped bordered hover style={{textAlign:"center"}}>
        <thead>
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {author.map((item, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{item._id}</td>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>
                <img src={item.image} alt="" width={150} height={100} />
              </td>
              <td>
                <Button
                  size="md"
                  variant="warning"
                  onClick={() => {
                    setIsUpdate(true);
                    tempId.current = item._id;
                    setName(item.name);
                    setEmail(item.email);
                    document.getElementById("name").value = item.name;
                    document.getElementById("email").value = item.email;
                    document.getElementById("image").src = item.image;
                  }}
                >
                  Edit
                </Button>{" "}
                <Button
                  size="md"
                  variant="danger"
                  onClick={() => onDelete(item._id)}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
